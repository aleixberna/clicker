﻿using Clicker.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Clicker.Services
{
    public class UserService
    {
        HttpClient http;
        Config config;
        public async Task<bool> Login(string user, string password)
        {
            bool result = false;

            http = new HttpClient();
            config = new Config();

            //especifico ruta y servicio
            var url = $"{Config.UrlBaseServer}/user?user={user}&password={password}";

            //reliza peticion
            var response = await http.GetAsync(url);

            if (response.IsSuccessStatusCode)
            {
                //se reciben los datos en un string JSon
                var content = await response.Content.ReadAsStringAsync();
                //se convierte a un objeto c#
                result = JsonConvert.DeserializeObject<bool>(content);
            }

            return result;
        }

        static HttpClient client = new HttpClient();
      //  static HttpContent content;
        public async Task<bool> Registre(User user)
        {
            try
            {
                User usuario = new User();
                bool result = false;

                var url = $"{Config.UrlBaseServer}/user?";
                var json = JsonConvert.SerializeObject(user);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                HttpResponseMessage response = await client.PostAsync(url, content);

                if (response.IsSuccessStatusCode)
                {
                    //se reciben los datos en un string JSon
                    var content2 = await response.Content.ReadAsStringAsync();
                    //se convierte a un objeto c#
                    result = JsonConvert.DeserializeObject<bool>(content2);
                }

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        



    }
}
