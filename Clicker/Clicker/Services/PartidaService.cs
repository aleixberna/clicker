﻿using Clicker.Services;
using Cliker.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Cliker.Services
{
    class PartidaService
    {
        HttpClient http;
        static HttpClient client = new HttpClient();
        static HttpContent content;

        public async void Datos(Partida partida)
        {
            try
            {
                bool result = false;

                var url = $"{Config.UrlBaseServer}/partida?";
                var json = JsonConvert.SerializeObject(partida);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                HttpResponseMessage response = await client.PostAsync(url, content);

                if (response.IsSuccessStatusCode)
                {
                    var content2 = await response.Content.ReadAsStringAsync();
                   // result = JsonConvert.DeserializeObject<bool>(content2);
                   
                }
                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
    }
}
