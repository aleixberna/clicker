﻿
using Clicker.Model;
using Clicker.Services;
using Cliker.Model;
using Cliker.Services;
using Rg.Plugins.Popup.Animations;
using Rg.Plugins.Popup.Enums;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Clicker
{
    public class MainPageViewModel : ContentPage, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        CultureInfo cultureInfo = CultureInfo.CreateSpecificCulture("el-GR");

        public ICommand BotonLogin { get; set; }
        public ICommand BotonRegistre { get; set; }
        public ICommand PulsacionesSuma { get; set; }
        public ICommand BotoMillora1 { get; set; }
        public ICommand EstatTresor1 { get; set; }
        public ICommand EstatTresor2 { get; set; }
        public ICommand EstatTresor3 { get; set; }
        public ICommand EstatTresor4 { get; set; }
        public ICommand EstatTresor5 { get; set; }
        public ICommand EstatTresor6 { get; set; }
        public ICommand EstatTresor7 { get; set; }
        public ICommand EstatTresor8 { get; set; }
        public ICommand EstatTresor9 { get; set; }
        public ICommand BotoMillora2 { get; set; }
        public ICommand BotoMillora3 { get; set; }
        public ICommand BotoReset { get; set; }
        public ICommand BotoTresors { get; set; }
        public ICommand ResetTresors { get; set; }
        public ICommand PulsacionImageRandom { get; set; }
        public ICommand PulsacionImageRandom2 { get; set; }

        public int numeroClicks = 0;
        public BigInteger flagNum = 0;
        public String flagNumString = "";
        public String numeroClicksString = "0";
        public int tapCount = 0;

        public int numMillora1 = 1;
        public int preuMillora1 = 10;
        public int numMillora2 = 1;
        public int preuMillora2 = 10;
        public int numMillora3 = 1;
        public int preuMillora3 = 10;
        public String resultatCadaTresor = "0";
        public String totalNumTresors = "0";
        public String numMillora1String = "01";
        public String preuMillora1String = "10";
        public String numMillora2String = "01";
        public String preuMillora2String = "10";
        public String numMillora3String = "01";
        public String preuMillora3String = "10";
        public int NumeroClicksInt = 0;
        public int randomHoritzontal = 0;
        public int randomVertical = 0;
        public int randomHoritzontal2 = 0;
        public int randomVertical2 = 0;
        public int resultatTresor1 = 0;
        public int resultatTresor2 = 0;
        public int resultatTresor3 = 0;
        public int randomOcultarImatge = 0;
        public int randomOcultarImatge2 = 0;
        public int contclaus = 1;
        public int contresorsoberts = 0;

        public int millon;
        public int billon;
        public int trillon;
        public int cuadrillon;
        public int quintillon;
        public int sextillon;
        public int septillon;
        public int octillon;
        public int nonillon;
        public int decallon;

        public Boolean autocont = true;
        public Boolean bonusTresors = false;
        public Boolean boolTotalTresors = false;
        public Boolean boolmostrarmoneda = true;
        public Boolean boolmoneda = false;
        public String bonusMonedaString = "cristal.jpg";
        public String imatgeTresor1 = "tresortancat.png";
        public String imatgeTresor2 = "tresortancat.png";
        public String imatgeTresor3 = "tresortancat.png";
        public String imatgeTresor4 = "tresortancat.png";
        public String imatgeTresor5 = "tresortancat.png";
        public String imatgeTresor6 = "tresortancat.png";
        public String imatgeTresor7 = "tresortancat.png";
        public String imatgeTresor8 = "tresortancat.png";
        public String imatgeTresor9 = "tresortancat.png";
        public String colorMillora1 = "White";
        public String colorMillora2 = "White";
        public String booleanImageInvisible = "False";
        public String booleanImageInvisible2 = "False";
        public String imatgeBoto = "pic1.png";
        public String imatgeClaus = "zeroclaus.png";
        public String name = "Nombre";
        public String surname = "Apellidos";
        public String email = "Email";
        public String user = "Usuario";
        public String pass = "Password";

        ContentPage nav;

        public String Name
        {
            set
            {
                if (name != value)
                {
                    name = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Name"));
                    }
                }
            }
            get { return name; }
        }
        public String Surname
        {
            set
            {
                if (surname != value)
                {
                    surname = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Surname"));
                    }
                }
            }
            get { return surname; }
        }
        public String Email
        {
            set
            {
                if (email != value)
                {
                    email = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Email"));
                    }
                }
            }
            get { return email; }
        }
        public String User
        {
            set
            {
                if (user != value)
                {
                    user = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("User"));
                    }
                }
            }
            get { return user; }
        }
        public String Pass
        {
            set
            {
                if (pass != value)
                {
                    pass = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Pass"));
                    }
                }
            }
            get { return pass; }
        }
        public int NumeroClicks { set { if (numeroClicks != value) { numeroClicks = value; if (PropertyChanged != null) {
                        PropertyChanged(this, new PropertyChangedEventArgs("NumeroClicks")); } } } get { return numeroClicks; }
        }
        public String NumeroClicksString { set { if (numeroClicksString != value) {
                    numeroClicksString = value; if (PropertyChanged != null) {
                        PropertyChanged(this, new PropertyChangedEventArgs("NumeroClicksString")); } } } get { return numeroClicksString; }
        }
        public int NumMillora1
        {
            set
            {
                if (numMillora1 != value)
                {
                    numMillora1 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("NumMillora1"));
                    }
                }
            }
            get { return numMillora1; }
        }
        public String TotalNumTresors
        {
            set
            {
                if (totalNumTresors != value)
                {
                    totalNumTresors = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TotalNumTresors"));
                    }
                }
            }
            get { return totalNumTresors; }
        }
        public String NumMillora1String
        {
            set
            {
                if (numMillora1String != value)
                {
                    numMillora1String = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("NumMillora1String"));
                    }
                }
            }
            get { return numMillora1String; }
        }
        public int PreuMillora1
        {
            set
            {
                if (preuMillora1 != value)
                {
                    preuMillora1 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("PreuMillora1"));
                    }
                }
            }
            get { return preuMillora1; }
        }
        public String PreuMillora1String
        {
            set
            {
                if (preuMillora1String != value)
                {
                    preuMillora1String = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("PreuMillora1String"));
                    }
                }
            }
            get { return preuMillora1String; }
        }
        public int NumMillora2
        {
            set
            {
                if (numMillora2 != value)
                {
                    numMillora2 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("NumMillora2"));
                    }
                }
            }
            get { return numMillora2; }
        }
        public String NumMillora2String
        {
            set
            {
                if (numMillora2String != value)
                {
                    numMillora2String = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("NumMillora2String"));
                    }
                }
            }
            get { return numMillora2String; }
        }
        public int PreuMillora2
        {
            set
            {
                if (preuMillora2 != value)
                {
                    preuMillora2 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("PreuMillora2"));
                    }
                }
            }
            get { return preuMillora2; }
        }
        public String PreuMillora2String
        {
            set
            {
                if (preuMillora2String != value)
                {
                    preuMillora2String = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("PreuMillora2String"));
                    }
                }
            }
            get { return preuMillora2String; }
        }
        public int NumMillora3
        {
            set
            {
                if (numMillora3 != value)
                {
                    numMillora3 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("NumMillora3"));
                    }
                }
            }
            get { return numMillora3; }
        }
        public String NumMillora3String
        {
            set
            {
                if (numMillora3String != value)
                {
                    numMillora3String = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("NumMillora3String"));
                    }
                }
            }
            get { return numMillora3String; }
        }
        public int PreuMillora3
        {
            set
            {
                if (preuMillora3 != value)
                {
                    preuMillora3 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("PreuMillora3"));
                    }
                }
            }
            get { return preuMillora3; }
        }
        public String PreuMillora3String
        {
            set
            {
                if (preuMillora3String != value)
                {
                    preuMillora3String = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("PreuMillora3String"));
                    }
                }
            }
            get { return preuMillora3String; }
        }
        public int RandomHoritzontal { set { if (randomHoritzontal != value) { randomHoritzontal = value; if (PropertyChanged != null) {
                        PropertyChanged(this, new PropertyChangedEventArgs("RandomHoritzontal")); } } } get { return randomHoritzontal; }
        }
        public int RandomVertical { set { if (randomVertical != value) { randomVertical = value; if (PropertyChanged != null) {
                        PropertyChanged(this, new PropertyChangedEventArgs("RandomVertical")); } } } get { return randomVertical; }
        }
        public int RandomHoritzontal2
        {
            set
            {
                if (randomHoritzontal2 != value)
                {
                    randomHoritzontal2 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("RandomHoritzontal2"));
                    }
                }
            }
            get { return randomHoritzontal2; }
        }
        public int RandomVertical2
        {
            set
            {
                if (randomVertical2 != value)
                {
                    randomVertical2 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("RandomVertical2"));
                    }
                }
            }
            get { return randomVertical2; }
        }
        public String ResultatCadaTresor
        {
            set
            {
                if (resultatCadaTresor != value)
                {
                    resultatCadaTresor = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ResultatCadaTresor"));
                    }
                }
            }
            get { return resultatCadaTresor; }
        }
        public String ImatgeTresor1
        {
            set
            {
                if (imatgeTresor1 != value)
                {
                    imatgeTresor1 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImatgeTresor1"));
                    }
                }
            }
            get { return imatgeTresor1; }
        }
        public String ImatgeTresor2
        {
            set
            {
                if (imatgeTresor2 != value)
                {
                    imatgeTresor2 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImatgeTresor2"));
                    }
                }
            }
            get { return imatgeTresor2; }
        }
        public String ImatgeTresor3
        {
            set
            {
                if (imatgeTresor3 != value)
                {
                    imatgeTresor3 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImatgeTresor3"));
                    }
                }
            }
            get { return imatgeTresor3; }
        }
        public String ImatgeTresor4
        {
            set
            {
                if (imatgeTresor4 != value)
                {
                    imatgeTresor4 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImatgeTresor4"));
                    }
                }
            }
            get { return imatgeTresor4; }
        }
        public String ImatgeTresor5
        {
            set
            {
                if (imatgeTresor5 != value)
                {
                    imatgeTresor5 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImatgeTresor5"));
                    }
                }
            }
            get { return imatgeTresor5; }
        }
        public String ImatgeTresor6
        {
            set
            {
                if (imatgeTresor6 != value)
                {
                    imatgeTresor6 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImatgeTresor6"));
                    }
                }
            }
            get { return imatgeTresor6; }
        }
        public String ImatgeTresor7
        {
            set
            {
                if (imatgeTresor7 != value)
                {
                    imatgeTresor7 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImatgeTresor7"));
                    }
                }
            }
            get { return imatgeTresor7; }
        }
        public String ImatgeTresor8
        {
            set
            {
                if (imatgeTresor8 != value)
                {
                    imatgeTresor8 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImatgeTresor8"));
                    }
                }
            }
            get { return imatgeTresor8; }
        }
        public String ImatgeTresor9
        {
            set
            {
                if (imatgeTresor9 != value)
                {
                    imatgeTresor9 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImatgeTresor9"));
                    }
                }
            }
            get { return imatgeTresor9; }
        }
        public Boolean BonusTresors
        {
            set
            {
                if (bonusTresors != value)
                {
                    bonusTresors = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("BonusTresors"));
                    }
                }
            }
            get { return bonusTresors; }
        }
        public Boolean BoolTotalTresors
        {
            set
            {
                if (boolTotalTresors != value)
                {
                    boolTotalTresors = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("BoolTotalTresors"));
                    }
                }
            }
            get { return boolTotalTresors; }
        }
        public String BonusMonedaString
        {
            set
            {
                if (bonusMonedaString != value)
                {
                    bonusMonedaString = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("BonusMonedaString"));
                    }
                }
            }
            get { return bonusMonedaString; }
        }
        public String BooleanImageInvisible
        {
            set
            {
                if (booleanImageInvisible != value)
                {
                    booleanImageInvisible = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("BooleanImageInvisible"));
                    }
                }
            }
            get { return booleanImageInvisible; }
        }
        public String BooleanImageInvisible2
        {
            set
            {
                if (booleanImageInvisible2 != value)
                {
                    booleanImageInvisible2 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("BooleanImageInvisible2"));
                    }
                }
            }
            get { return booleanImageInvisible2; }
        }
        public String ImatgeBoto { set { if (imatgeBoto != value) { imatgeBoto = value; if (PropertyChanged != null) {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImatgeBoto")); } } } get { return imatgeBoto; }
        }
        public String ImatgeClaus
        {
            set
            {
                if (imatgeClaus != value)
                {
                    imatgeClaus = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImatgeClaus"));
                    }
                }
            }
            get { return imatgeClaus; }
        }
        public String ColorMillora1
        {
            set
            {
                if (colorMillora1 != value)
                {
                    colorMillora1 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ColorMillora1"));
                    }
                }
            }
            get { return colorMillora1; }
        }
        public String ColorMillora2
        {
            set
            {
                if (colorMillora2 != value)
                {
                    colorMillora2 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ColorMillora2"));
                    }
                }
            }
            get { return colorMillora2; }
        }
        public MainPageViewModel(ContentPage navi)
        {
            nav = navi;
            BotoMillora1 = new Command(Millora1);
            BotoMillora2 = new Command(Millora2);
            BotoMillora2 = new Command(Millora3);

            EstatTresor1 = new Command(Tresor1);
            EstatTresor2 = new Command(Tresor2);
            EstatTresor3 = new Command(Tresor3);
            EstatTresor4 = new Command(Tresor4);
            EstatTresor5 = new Command(Tresor5);
            EstatTresor6 = new Command(Tresor6);
            EstatTresor7 = new Command(Tresor7);
            EstatTresor8 = new Command(Tresor8);
            EstatTresor9 = new Command(Tresor9);
            BotonLogin = new Command(IniciarSesion);
            BotonRegistre = new Command(Registre);

            BotoReset = new Command(ReiniciarVariables);
            ResetTresors = new Command(ReiniciarTresors);
            PulsacionesSuma = new Command(Clicksuma);
            PulsacionImageRandom = new Command(ImageRandomClick);
            PulsacionImageRandom2 = new Command(ImageRandomClick2);

            RandomImage();
            BooleanImage();
            RandomImage2();
            BooleanImage2();

        }

        private async Task SumasolaAsync()
        {

            while (autocont == true)
            {

                NumeroClicks = NumeroClicks + NumMillora2;
                NumeroClicksString = ComprovarNum(NumeroClicks);

                await Task.Delay(1000);
            }

        }
        public void Clicksuma()
        {
            //Aqui llegaré quando pulse la imagen
            NumeroClicks = NumeroClicks + NumMillora1;

            if (boolmoneda == true)
            {
                NumeroClicks = NumeroClicks + ((int)NumeroClicks / 4);
            }


            NumeroClicksString = ComprovarNum(NumeroClicks);



            //sharepreference

            ImatgePic();

        }
        public String ComprovarNum(BigInteger flagNum)
        {
            if (flagNum < 1000000)
            {
                flagNumString = flagNum.ToString("0,0", cultureInfo);
            }
            else if (flagNum >= 1000000 && flagNum < 10000000)
            {
                flagNumString = (flagNum / 1000).ToString("0,0M", cultureInfo);
            }
            else if (flagNum >= 10000000 && flagNum < 100000000)
            {
                flagNumString = (flagNum / 1000).ToString("0,0M", cultureInfo);
            }
            else if (flagNum >= 100000000 && flagNum < 1000000000)
            {
                flagNumString = (flagNum / 1000).ToString("0,0M", cultureInfo);
            }
            else if (flagNum >= 1000000000 && flagNum < 10000000000)
            {
                flagNumString = (flagNum / 1000000000).ToString("0,000B", cultureInfo);
            }
            else if (flagNum >= 10000000000 && flagNum < 100000000000)
            {
                flagNumString = (flagNum / 1000000000).ToString("0,000B", cultureInfo);
            }
            else if (flagNum >= 100000000000 && flagNum < 1000000000000)
            {
                flagNumString = (flagNum / 1000000000).ToString("0,000B", cultureInfo);
            }
            else if (flagNum >= 1000000000000 && flagNum < 10000000000000)
            {
                flagNumString = (flagNum / 1000000000000000000).ToString("0,000T", cultureInfo);
            }
            else if (flagNum >= 10000000000000 && flagNum < 100000000000000)
            {
                flagNumString = (flagNum / 1000000000000000000).ToString("0,000T", cultureInfo);
            }
            else if (flagNum >= 100000000000000 && flagNum < 1000000000000000)
            {
                flagNumString = (flagNum / 1000000000000000000).ToString("0,000T", cultureInfo);
            }

            return flagNumString;
        }
        public void ImageRandomClick()
        {
            NumeroClicks = NumeroClicks + (NumMillora1 * 5);
            NumeroClicksString = ComprovarNum(NumeroClicks);
        }
        public void ImageRandomClick2()
        {
            boolmoneda = true;
            BonusMonedaString = "diamante.jpg";
        }
        public void ImageClausChange() {
            if (contclaus == 1)
            {
                ImatgeClaus = "unaclau.png";
            }
            else if (contclaus == 2)
            {
                ImatgeClaus = "dosclaus.png";
            }
            else if (contclaus == 3)
            {
                ImatgeClaus = "tresclaus.png";
                contclaus = 0;
                BonusTresors = true;
            }
            contclaus++;
        }
        public void CalculsTresors()
        {
            Random rand = new Random();
            NumeroClicksInt = (int)NumeroClicks / 2;
            resultatTresor1 = rand.Next(0, NumeroClicksInt);
            //resultatTresor2 = rand.Next(0, 150);
            //resultatTresor3 = rand.Next(0, 150);
            ResultatCadaTresor = resultatTresor1.ToString();
            resultatTresor2 = resultatTresor2 + resultatTresor1;
            if ( contresorsoberts == 2 ) { CalcularTotalTresors(); }
        }
        public void CalcularTotalTresors()
        { 
            TotalNumTresors = resultatTresor2.ToString();
            BoolTotalTresors = true;
            NumeroClicks = NumeroClicks + resultatTresor2;
            NumeroClicksString = ComprovarNum(NumeroClicks);
        }
        public void ReiniciarTresors()
        {
            ImatgeTresor1 = "tresortancat.png"; ImatgeTresor2 = "tresortancat.png"; ImatgeTresor3 = "tresortancat.png";
            ImatgeTresor4 = "tresortancat.png"; ImatgeTresor5 = "tresortancat.png"; ImatgeTresor6 = "tresortancat.png";
            ImatgeTresor7 = "tresortancat.png"; ImatgeTresor8 = "tresortancat.png"; ImatgeTresor9 = "tresortancat.png";
            imatgeClaus = "zeroclaus.png";
            contresorsoberts = 0;
            boolTotalTresors = false;
            resultatTresor2 = 0;
            resultatTresor1 = 0;
            resultatCadaTresor = "0";
            totalNumTresors = "0";
            BonusTresors = false;
            contclaus = 1;
        }
        private async Task RandomImage()
        {
            Random rand = new Random();

            while (true)
            {
                RandomHoritzontal = rand.Next(-150, 150);
                RandomVertical = rand.Next(-650, 50);
                await Task.Delay(1000);
            }
        }
        private async Task RandomImage2()
        {
            Random rand = new Random();

            while (true)
            {
                RandomHoritzontal2 = rand.Next(-150, 150);
                RandomVertical2 = rand.Next(-650, 50);
                await Task.Delay(1000);
            }
        }
        private async Task ImatgePic()
        {
            ImatgeBoto = "pic1.png";
            await Task.Delay(100);
            ImatgeBoto = "pic2.png";
            await Task.Delay(100);
            ImatgeBoto = "pic3.png";
            await Task.Delay(100);
            ImatgeBoto = "pic4.png";
            await Task.Delay(100);
            ImatgeBoto = "pic1.png";
            await Task.Delay(100);

        }
        private async Task BooleanImage()
        {
            Random rand = new Random();

            while (true)
            {
                await Task.Delay(5000);
                randomOcultarImatge = rand.Next(0, 10);
                //randomOcultarImatge = 1;

                if (randomOcultarImatge == 1)
                {
                    BooleanImageInvisible = "True";
                }
                else
                {
                    BooleanImageInvisible = "False";
                }
            }
        }
        private async Task BooleanImage2()
        {
            Random rand = new Random();

            while (true)
            {
                await Task.Delay(1000);
                randomOcultarImatge2 = rand.Next(0, 10);
                //randomOcultarImatge = 1;
                    
                if (randomOcultarImatge2 == 1)
                {
                    BooleanImageInvisible2 = "True";
                    await Task.Delay(1000);
                    BooleanImageInvisible2 = "False";
                    await Task.Delay(29000);
                    BonusMonedaString = "cristal.jpg";
                    boolmoneda = false;
                }
                else
                {
                    BooleanImageInvisible2 = "False";
                }

            }
        }
        public void Millora1()
        {
            if (NumeroClicks >= PreuMillora1)
            {

                NumeroClicks = NumeroClicks - PreuMillora1;
                NumeroClicksString = ComprovarNum(NumeroClicks);
                NumMillora1++;
                NumMillora1String = ComprovarNum(NumMillora1);
                PreuMillora1 = PreuMillora1 * 2;
                PreuMillora1String = ComprovarNum(PreuMillora1);

                ImageClausChange();
            }
            else
            {
                Application.Current.MainPage.DisplayAlert("FLIPAAAAAAAT!!", "Fes més clicks RUC!", "Bueno...");
                //PopUp1();
            }

            

        }
        public void Millora2()
        {

            if (NumeroClicks >= PreuMillora2)
            {
                autocont = true;
                if (PreuMillora2 == 10)
                {
                    SumasolaAsync();
                }
                NumeroClicks = NumeroClicks - PreuMillora2;
                NumeroClicksString = ComprovarNum(NumeroClicks);
                NumMillora2++;
                NumMillora2String = ComprovarNum(NumMillora2);
                PreuMillora2 = PreuMillora2 * 2;
                PreuMillora2String = ComprovarNum(PreuMillora2);
            }
            else
            {
                Application.Current.MainPage.DisplayAlert("FLIPAAAAAAAT!!", "Fes més clicks RUC!", "Bueno...");
            }

        }
        public void Millora3()
        {

            if (NumeroClicks >= PreuMillora3)
            {
                autocont = true;
                //if (PreuMillora3 == 10)
                //{
                //    SumasolaAsync();
                //}
                NumeroClicks = NumeroClicks - PreuMillora3;
                NumeroClicksString = ComprovarNum(NumeroClicks);
                NumMillora3++;
                NumMillora3String = ComprovarNum(NumMillora3);
                PreuMillora3 = PreuMillora3 * 2;
                PreuMillora3String = ComprovarNum(PreuMillora3);
            }
            else
            {
                Application.Current.MainPage.DisplayAlert("FLIPAAAAAAAT!!", "Fes més clicks RUC!", "Bueno...");
            }

        }
        public void Tresor1()
        {
            if (ImatgeTresor1 == "tresortancat.png" && contresorsoberts < 3) { CalculsTresors(); ImatgeTresor1 = "tresorobert.png"; contresorsoberts++; }
        }
        public void Tresor2()
        {
            if (ImatgeTresor2 == "tresortancat.png" && contresorsoberts < 3) { CalculsTresors(); ImatgeTresor2 = "tresorobert.png"; contresorsoberts++; }
        }
        public void Tresor3()
        {
            if (ImatgeTresor3 == "tresortancat.png" && contresorsoberts < 3) { CalculsTresors(); ImatgeTresor3 = "tresorobert.png"; contresorsoberts++; }
        }
        public void Tresor4()
        {
            if (ImatgeTresor4 == "tresortancat.png" && contresorsoberts < 3) { CalculsTresors(); ImatgeTresor4 = "tresorobert.png"; contresorsoberts++; }
        }
        public void Tresor5()
        {
            if (ImatgeTresor5 == "tresortancat.png" && contresorsoberts < 3) { CalculsTresors(); ImatgeTresor5 = "tresorobert.png"; contresorsoberts++; }
        }
        public void Tresor6()
        {
            if (ImatgeTresor6 == "tresortancat.png" && contresorsoberts < 3) { CalculsTresors(); ImatgeTresor6 = "tresorobert.png"; contresorsoberts++; }
        }
        public void Tresor7()
        {
            if (ImatgeTresor7 == "tresortancat.png" && contresorsoberts < 3) { CalculsTresors(); ImatgeTresor7 = "tresorobert.png"; contresorsoberts++; }
        }
        public void Tresor8()
        {
            if (ImatgeTresor8 == "tresortancat.png" && contresorsoberts < 3) { CalculsTresors(); ImatgeTresor8 = "tresorobert.png"; contresorsoberts++; }
        }
        public void Tresor9()
        {
            if (ImatgeTresor9 == "tresortancat.png" && contresorsoberts < 3) { CalculsTresors(); ImatgeTresor9 = "tresorobert.png"; contresorsoberts++; }
        }
        public void ReiniciarVariables()
        {
            DatosPartida();
            NumeroClicks = 0;
            NumMillora1 = 1;
            PreuMillora1 = 10;
            NumMillora2 = 1;
            PreuMillora2 = 10;
            contclaus = 0;
            NumMillora1String = "01";
            PreuMillora1String = "10";
            NumMillora2String = "01";
            PreuMillora2String = "10";
            NumeroClicksString = "0";
            ColorMillora1 = "White";
            ColorMillora2 = "White";
            autocont = false;
            ImatgeTresor1 = "tresortancat.png"; ImatgeTresor2 = "tresortancat.png"; ImatgeTresor3 = "tresortancat.png";
            ImatgeTresor4 = "tresortancat.png"; ImatgeTresor5 = "tresortancat.png"; ImatgeTresor6 = "tresortancat.png";
            ImatgeTresor7 = "tresortancat.png"; ImatgeTresor8 = "tresortancat.png"; ImatgeTresor9 = "tresortancat.png";
            imatgeClaus = "zeroclaus.png";
            contresorsoberts = 0;
            boolTotalTresors = false;
            resultatTresor2 = 0;
            resultatTresor1 = 0;
            resultatCadaTresor = "0";
            totalNumTresors = "0";
            BonusTresors = false;
            contclaus = 1;

        }

        //private async void PopUp1()
        //{
        //    var pr = new PopUp();

        //    var scaleAnimation = new ScaleAnimation
        //    {
        //        PositionIn = MoveAnimationOptions.Right,
        //        PositionOut = MoveAnimationOptions.Left
        //    };

        //    pr.Animation = scaleAnimation;

        //    await PopupNavigation.PushAsync(pr);

        //}

        private async void IniciarSesion()
        {
            UserService service = new UserService();
            Boolean bol = true;
            if (String.IsNullOrWhiteSpace(User))
            {
                await this.DisplayAlert("Advertencia", "Este campo es obligatorio", "Cerrar");
                bol = false;
            }
            if (String.IsNullOrWhiteSpace(Pass))
            {
                await this.DisplayAlert("Advertencia", "Este campo es obligatorio", "Cerrar");
                bol = false;
            }
            if (bol == true)
            {
                bool isLogued = await service.Login(User, Pass);

                if (isLogued)
                {
                    await nav.Navigation.PushModalAsync(new MainPage(this));
                }
            }
        }

        private async void Registre()
        {
            UserService service = new UserService();
            Boolean bol = true;
            if (String.IsNullOrWhiteSpace(Name))
            {
                await this.DisplayAlert("Advertencia", "Este campo es obligatorio", "Cerrar");
                bol = false;
            }
            if (String.IsNullOrWhiteSpace(Surname))
            {
                await this.DisplayAlert("Advertencia", "Este campo es obligatorio", "Cerrar");
                bol = false;
            }
            if (String.IsNullOrWhiteSpace(Email))
            {
                await this.DisplayAlert("Advertencia", "Este campo es obligatorio", "Cerrar");
                bol = false;
            }
            if (String.IsNullOrWhiteSpace(User))
            {
                await this.DisplayAlert("Advertencia", "Este campo es obligatorio", "Cerrar");
                bol = false;
            }
            if (String.IsNullOrWhiteSpace(Pass))
            {
                await this.DisplayAlert("Advertencia", "Este campo es obligatorio", "Cerrar");
                bol = false;
            }

            User usuario = new User();
            usuario.Nombre = Name;
            usuario.Apellido = Surname;
            usuario.Email = Email;
            usuario.Usuario = User;
            usuario.Password = Pass;


            if (bol == true)
            {
                bool isLogued = await service.Registre(usuario);

                if (isLogued)
                {
                    await nav.Navigation.PushModalAsync(new Login(this));
                }
            }
        }

        private async void DatosPartida()
        {
            PartidaService service = new PartidaService();

            Partida usuario = new Partida();
            usuario.Usuario = User;
            usuario.punts = NumeroClicks;
            usuario.cont = NumMillora1;
            usuario.autocont = NumMillora2;
            usuario.dinamita = PreuMillora1;
            usuario.escavadora = PreuMillora2;
            usuario.petrolera = PreuMillora3;
            usuario.fabrica = PreuMillora3;
            service.Datos(usuario);
        }


    }
}