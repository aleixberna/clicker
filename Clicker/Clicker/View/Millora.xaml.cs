﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Clicker
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Millora : ContentPage
    {
        public Millora(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;
        }

        private void MainPage(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new MainPage(BindingContext));
        }

        private void Tresors(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Tresors(BindingContext));
        }


    }
}