﻿using Clicker.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Clicker
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {
        public Login()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);

        }
        public Login(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;
        }

        private void MainPage(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new MainPage(BindingContext));
        }
        private void Registre(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Registre(BindingContext));
        }
    }
}