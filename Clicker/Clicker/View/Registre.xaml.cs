﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Clicker.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Registre : ContentPage
    {
        public Registre()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);

        }
        public Registre(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;
        }

        private void MainPage(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new MainPage(BindingContext));
        }
    }
}