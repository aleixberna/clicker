﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Clicker.Model
{
    public class User
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Email { get; set; }
        public string Usuario { get; set; }
        public string Password { get; set; }
    }

}
