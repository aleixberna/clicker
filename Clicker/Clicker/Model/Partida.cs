﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cliker.Model
{
    public class Partida
    {
            public int punts { get; set; }
            public int autocont { get; set; }
            public int cont { get; set; }
            public int dinamita { get; set; }
            public int escavadora { get; set; }
            public int petrolera { get; set; }
            public int fabrica { get; set; }
            public string Usuario { get; set; }

    }
}
